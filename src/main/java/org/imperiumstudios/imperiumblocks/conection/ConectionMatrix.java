/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Imperium Studios <https://imperiumstudios.org>
 * Copyright (c) 2016 garantiertnicht <>
 * Copyright (c) 2016 Kevin Olinger <https://kevinolinger.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.imperiumstudios.imperiumblocks.conection;

public class ConectionMatrix {
    public final boolean up;
    public final boolean down;
    public final boolean east;
    public final boolean west;
    public final boolean north;
    public final boolean south;

    public ConectionMatrix(boolean down, boolean up, boolean north, boolean south, boolean west, boolean east) {
        this.up = up;
        this.down = down;
        this.east = east;
        this.west = west;
        this.north = north;
        this.south = south;
    }

    public ConectionMatrixSide up() {
        return new ConectionMatrixSide(north, south, west, east);
    }

    //TODO
    public ConectionMatrixSide down() {
        return new ConectionMatrixSide(north, south, east, west);
    }

    public ConectionMatrixSide north() {
        return new ConectionMatrixSide(up, down, west, east);
    }

    public ConectionMatrixSide south() {
        return new ConectionMatrixSide(up, down, west, east);
    }

    public ConectionMatrixSide east() {
        return new ConectionMatrixSide(up, down, south, north);
    }

    public ConectionMatrixSide west() {
        return new ConectionMatrixSide(up, down, north, south);
    }
}
