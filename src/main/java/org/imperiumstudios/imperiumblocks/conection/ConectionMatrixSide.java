/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Imperium Studios <https://imperiumstudios.org>
 * Copyright (c) 2016 garantiertnicht <>
 * Copyright (c) 2016 Kevin Olinger <https://kevinolinger.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.imperiumstudios.imperiumblocks.conection;

public class ConectionMatrixSide {
    public final boolean up;
    public final boolean down;
    public final boolean left;
    public final boolean right;

    public final int icon;
    public final int rotation;

    public ConectionMatrixSide(boolean up, boolean down, boolean left, boolean right) {
        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;

        icon = getIcon();
        rotation = getRotation();
    }

    private int getIcon() {
        if(!up && !down && !left && !right) {
            return 0;
        } else if(!up && !down && !left) {
            return 1;
        } else if(!up && !down && !right) {
            return 1;
        } else if(!up && !down) {
            return 2;
        } else if(!up && !left && !right) {
            return 1;
        } else if(!up && !left) {
            return 3;
        } else if(!up && !right) {
            return 3;
        } else if(!up) {
            return 4;
        } else if(!down && !left && !right) {
            return 1;
        } else if(!down && !left) {
            return 3;
        } else if(!down && right) {
            return 4;
        } else if(!down) {
            return 3;
        } else if(!left && !right) {
            return 2;
        } else if(!left) {
            return 4;
        } else if (!right) {
            return 4;
        }
        else {
            return 5;
        }
    }

    private int getRotation() {
        if(!up && down && left && !right || !up && down && !left && !right || up && down && left && !right) {
            return 1;
        } else if(up && !down && !left || up && down && !left && !right || up && down && !left) {
            return 2;
        } else if(!up && !down && left && !right || up && !down && right || up && !down) {
            return 3;
        }

        return 0;
    }
}
