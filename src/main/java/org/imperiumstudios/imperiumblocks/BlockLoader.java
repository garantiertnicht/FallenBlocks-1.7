/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Imperium Studios <https://imperiumstudios.org>
 * Copyright (c) 2016 garantiertnicht <>
 * Copyright (c) 2016 Kevin Olinger <https://kevinolinger.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.imperiumstudios.imperiumblocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import org.imperiumstudios.imperiumblocks.models.GenericBlock;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class BlockLoader {
    public final BlockUtils blockUtils;
    private static ImperiumBlocks Core;

    /**
     * The block Type passed to the constructor wasn't found.
     */
    public class InvalidBlockTypeException extends Exception {
        private InvalidBlockTypeException(String msg) {
            super(String.format("%s/%s: %s", blockUtils.name, blockUtils.type, msg), null, false, true);
        }

        private InvalidBlockTypeException(String msg, Throwable cause) {
            super(String.format("%s/%s: %s", blockUtils.name, blockUtils.type, msg), cause, false, true);
        }
    }

    public class BlockNameNotLowercaseException extends Exception {
        private BlockNameNotLowercaseException(String msg) {
            super(String.format("%s/%s: %s", blockUtils.name, blockUtils.type, msg), null, false, true);
        }

        private BlockNameNotLowercaseException(String msg, Throwable cause) {
            super(String.format("%s/%s: %s", blockUtils.name, blockUtils.type, msg), cause, false, true);
        }
    }

    /**
     * Creates a new Block
     * @param type The type of the Block. The Class will be org.imperiumstudios.fallenblocks.implementation.models.IMP(TYPE)
     *             with type lowercase EXCEPT the first letter.
     * @param name The name of the new Block.
     * @param blockProperties The block Properties to apply
     * @throws InvalidBlockTypeException If the type was invalid
     */
    protected BlockLoader(String type, String name, Properties blockProperties) throws InvalidBlockTypeException, BlockNameNotLowercaseException {
        char first = type.charAt(0);
        type = Character.toUpperCase(first) + type.substring(1).toLowerCase();

        blockUtils = new BlockUtils(Core, Boolean.valueOf(blockProperties.getProperty("log")), name, type);

        if(!name.equals(name.toLowerCase())) {
            throw new BlockNameNotLowercaseException("Names are required to be all lowercase!");
        }

        if(blockUtils.devLog) {
            blockUtils.info("Logging enabled");
        }

        Object o;
        Class blockClass = null;

        for(ModelRegister reg : ModelRegister.registers) {
            try {
                blockClass = Class.forName(String.format("%s.%s", reg.packagePath, type));
            } catch(ClassNotFoundException ignored) {};

            if(blockClass != null) {
                break;
            }

            break;
        }

        if(blockClass == null) {
            throw new InvalidBlockTypeException(String.format("There is no such Block Type called %s!", type));
        }

        Class inter[] = blockClass.getInterfaces();
        boolean flag = false;

        for(Class a : inter) {
            if(a == GenericBlock.class)
                flag = true;
            break;
        }

        if(!flag) {
            throw new InvalidBlockTypeException("Class exists but is no GenericBlock!");
        }

        flag = false;
        Class temp = blockClass;

        while(temp != Object.class) {
            temp = temp.getSuperclass();
            if(temp == Block.class) {
                flag = true;
                break;
            }
        }

        if(!flag)
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but is no instanceof Block!");

        try {
            o = blockClass.getConstructor(Properties.class, blockUtils.getClass()).newInstance(blockProperties, blockUtils);
        } catch (InstantiationException e) {
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but the construction failed!");
        } catch (IllegalAccessException e) {
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but the construction can not called (change to public)!");
        } catch (InvocationTargetException e) {
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but the construction thrown an error!", e);
        } catch (NoSuchMethodException e) {
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but has no matching " +
                "Constructor (Properties, BlockLoader)!");
        }

        if(blockUtils.devLog)
            blockUtils.info("Block successfully initalized!");

        GenericBlock genericBlock = (GenericBlock) o;
        Block block = (Block) o;

        blockUtils.bind(genericBlock, block);

        block.setBlockName(blockUtils.registryName);
        block.setStepSound(blockUtils.getSoundType(blockProperties.getProperty("sound", "stone")));

        try {
            block.setHardness(Float.valueOf(blockProperties.getProperty("hardness", "2")));
        } catch (NumberFormatException exc) {
            blockUtils.warn("Value in hardness is NO parsable Float!");
            block.setHardness(2);
        }

        if(blockProperties.getProperty("blast") != null)
            try {
                block.setResistance(Float.valueOf(blockProperties.getProperty("blast")));
            } catch (NumberFormatException exc) {
                blockUtils.warn("Value in blast is NO parsable Float!");
            }

        if(block.isOpaqueCube())
            try {
                block.setLightLevel(Float.valueOf(blockProperties.getProperty("light", "0.0F")));
            } catch (NumberFormatException exc) {
                blockUtils.warn("Value in light is NO parsable Float!");
            }

        GameRegistry.registerBlock(block, blockUtils.registryName);

        if(genericBlock.getItem() != null) {
            Item item = genericBlock.getItem();
            String itemName = blockUtils.registryName + '_' + "item";

            item.setUnlocalizedName(itemName);
            GameRegistry.registerItem(item, itemName);
        }

        if(blockUtils.devLog)
            blockUtils.info("Block Registered!");
    }

    public BlockLoader(BlockLoader parent, Object blockObject, String appendix, Properties blockProps) {
        blockUtils = new BlockUtils(Core, parent.blockUtils.devLog, parent.blockUtils.name + '_' + appendix, parent.blockUtils.type);
        GenericBlock genericBlock = (GenericBlock) blockObject;
        Block block = (Block) blockObject;

        blockUtils.bind(genericBlock, block);


        block.setBlockName(blockUtils.registryName);
        block.setStepSound(blockUtils.getSoundType(blockProps.getProperty("sound", "stone")));

        try {
            block.setHardness(Float.valueOf(blockProps.getProperty("hardness", "2")));
        } catch (NumberFormatException exc) {
            blockUtils.warn("Value in hardness is NO parsable Float!");
            block.setHardness(2);
        }

        if(blockProps.getProperty("blast") != null)
            try {
                block.setResistance(Float.valueOf(blockProps.getProperty("blast")));
            } catch (NumberFormatException exc) {
                blockUtils.warn("Value in blast is NO parsable Float!");
            }

        if(block.isOpaqueCube())
            try {
                block.setLightLevel(Float.valueOf(blockProps.getProperty("light", "0.0F")));
            } catch (NumberFormatException exc) {
                blockUtils.warn("Value in light is NO parsable Float!");
            }

        GameRegistry.registerBlock(block, blockUtils.registryName);

        if(genericBlock.getItem() != null) {
            Item item = genericBlock.getItem();
            String itemName = blockUtils.registryName + '_' + "item";

            item.setUnlocalizedName(itemName);
            GameRegistry.registerItem(item, itemName);
        }

        if(blockUtils.devLog) {
            blockUtils.info("Block Registered!");
        }
    }

    /**
     * Sets the Core. Seemnd to be better then having another var every call.
     * @param newCore FallenBlocks, wish is *HOPEFULLY* calling this.
     */
    static void init(ImperiumBlocks newCore) {
        Core = newCore;
    }
}