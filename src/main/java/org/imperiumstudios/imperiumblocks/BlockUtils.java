/*
 * Copyright (c) 2017, garantiertnicht
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by garantiertnicht Weichware.
 * 4. Neither the name of garantiertnicht Weichware nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY GARANTIERTNICHT WEICHWARE ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GARANTIERTNICHT WEICHWARE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.imperiumstudios.imperiumblocks;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import org.imperiumstudios.imperiumblocks.models.GenericBlock;

/**
 * Minecraft Version Specific Block Code.
 */
public class BlockUtils {
    private final ImperiumBlocks Core;
    public final boolean devLog;
    public final String name;
    public final String type;
    public final String registryName;

    private GenericBlock genericBlock;
    private Block block;

    public BlockUtils(ImperiumBlocks core, boolean devLog, String name, String type) {
        Core = core;
        this.devLog = devLog;
        this.name = name;
        this.type = type;

        this.registryName = String.format("%s_%s", name.toLowerCase(), type.toLowerCase());
    }

    public void bind(GenericBlock genericBlock, Block block) {
        this.genericBlock = genericBlock;
        this.block = block;
    }

    /**
     * Gets the full qualified Texture name. Will call {@link BlockUtils#getTextureName(String)} with the block name.
     * @deprecated You should not call this Variant if not nercassary. Use {@link BlockUtils#getTextureName()} instead.
     * @return A full qualified Texture name
     */
    @Deprecated
    public String getTextureNameNoExc() {
        return getTextureNameNoExc(name);
    }

    /**
     * Gets the full qualified Texture name for a subtexture.
     * @deprecated You should not call this Variant if not nercassary. Use {@link BlockUtils#getTextureName(String)} instead.
     * @param subName The name of the subtexture
     * @return A full qualified Texture name
     */
    @Deprecated
    public String getTextureNameNoExc(String subName) {
        if(devLog)
            try {
                info("Loading texture " + subName);
                return getTextureName(subName);
            } catch (NoSuchTexture noSuchTexture) {
                warn("Texture " + subName + " not found!");
            }

        return ImperiumBlocks.MODID +":"+ name +"/"+ subName;
    }

    /**
     * Gets the full qualified Texture name. Will call {@link BlockUtils#getTextureName(String)} with the block name.
     * @return A full qualified Texture name
     * @throws NoSuchTexture If the texture does not exist
     */
    public String getTextureName() throws NoSuchTexture {
        return getTextureName(name);
    }

    /**
     * Gets the full qualified Texture name for a subtexture.
     * @param subName The name of the subtexture
     * @return A full qualified Texture name
     * @throws NoSuchTexture If the texture does not exist
     */
    public String getTextureName(String subName) throws NoSuchTexture {
        if(devLog)
            info("Loading texture " + subName);

        List<String> textures = new ArrayList<>();
        try {
            textures.addAll(Arrays.asList(Core.jarUtils.getResourceFolderContent("assets/" + ImperiumBlocks.MODID + "/textures/blocks/" + name)));
        } catch (IOException | URISyntaxException | UnsupportedOperationException e) {
            throw new NoSuchTexture(ImperiumBlocks.MODID +":"+ name +"/"+ subName, e);
        }

        if(textures.contains(subName + ".png")) {
            if(devLog)
                info("Texture is " + ImperiumBlocks.MODID +":"+ name +"/"+ subName);
            return ImperiumBlocks.MODID +":"+ name +"/"+ subName;
        }
        else {
            if(devLog)
                warn("Texture not found!");
            throw new NoSuchTexture(ImperiumBlocks.MODID +":"+ name +"/"+ subName);
        }
    }

    /**
     * Register one Texture.
     * @param iconReg The IIcon register
     * @return The redy-to-use-and-registered IIcon
     * @throws NoSuchTexture If the texure is missing.
     */
    public IIcon registerIcons(IIconRegister iconReg) throws NoSuchTexture {
        if(devLog)
            info("Loading texture");
        return iconReg.registerIcon(getTextureName());
    }

    /**
     * Register multiple textures. If one of the subName is not available, it will try to fill all with BLOCKNAME.png
     * @param iconReg The IIcon register
     * @param subNames All subnames in order
     * @return The registered Icons
     * @throws NoSuchTexture If one ore more subName-textures fails loading AND there also is no BLOCKNAME.png
     */
    public IIcon[] registerIcons(IIconRegister iconReg, String... subNames) throws NoSuchTexture {
        IIcon icons[] = new IIcon[subNames.length];
        int i = 0;
        try {
            if(devLog)
                info("Try loading multiside textures...");
            for(String subName : subNames) {
                icons[i++] = iconReg.registerIcon(getTextureName(subName));
            }
        } catch (NoSuchTexture noSuchTexture) {
            if(devLog)
                warn("Texture " + subNames[i] + " failed to load! Fallback to normal textures.");
            for(i = 0; i < icons.length; i++) {
                icons[i] = iconReg.registerIcon(getTextureName());
            }
        }

        if(devLog)
            info("Done!");
        return icons;
    }

    /**
     * Gets the Item for a block.
     * @return An Item. Who is surprised?
     */
    public Item getItem() {
        return genericBlock.getItem() == null ? Item.getItemFromBlock(block) : genericBlock.getItem();
    }

    public void info(String msg) {
        ImperiumBlocks.log.info(String.format("%s/%s: %s", name, type, msg));
    }

    public void warn(String msg) {
        ImperiumBlocks.log.warn(String.format("%s/%s: %s", name, type, msg));
    }

    /**
     * Looks up an Material from a given String.
     * @param s The String-Representation of the Material.
     * @return Even more surprisingly, an Material!
     */
    public static Material getMaterial(String s) {
        String materialS[]  = {"air", "grass", "ground", "wood", "rock", "iron", "anvil", "water", "lava", "leaves", "plants", "vine", "sponge", "cloth",
            "fire", "sand", "circuits", "carpet", "glass", "redstoneLight", "tnt", "coral", "ice", "packedIce", "snow", "craftedSnow", "cactus",
            "clay", "gourd", "dragonEgg", "portal", "cake", "web", "piston"};

        Material material[] = {Material.air, Material.grass, Material.ground, Material.wood, Material.rock, Material.iron, Material.anvil, Material.water,
            Material.lava, Material.leaves, Material.plants, Material.vine, Material.sponge, Material.cloth, Material.fire, Material.sand,
            Material.circuits, Material.carpet, Material.glass, Material.redstoneLight, Material.tnt, Material.coral, Material.ice, Material.packedIce,
            Material.snow, Material.craftedSnow, Material.cactus, Material.clay, Material.gourd, Material.dragonEgg, Material.portal, Material.cake,
            Material.web, Material.piston};

        for(int i = 0; i < Math.min(materialS.length, material.length); i++) {
            if (materialS[i].equalsIgnoreCase(s))
                return material[i];
        }

        return Material.rock;
    }

    /**
     * Looks up an SoundType from a given String.
     * @param s The String-Representation of the SoundType.
     * @return A small bananna ridden by a giant ape, falling because of damn grass.
     */
    public static Block.SoundType getSoundType(String s) {
        String soundS[] = {"stone", "wood", "gravel", "grass", "piston", "metal", "glass", "cloth", "sand", "snow", "ladder"};
        Block.SoundType sound[] = {Block.soundTypeStone, Block.soundTypeWood, Block.soundTypeGravel, Block.soundTypeGrass, Block.soundTypePiston,
            Block.soundTypeMetal, Block.soundTypeGlass, Block.soundTypeCloth, Block.soundTypeSand, Block.soundTypeSnow, Block.soundTypeLadder};

        for (int i = 0; i < Math.min(soundS.length, sound.length); i++) {
            if (soundS[i].equalsIgnoreCase(s))
                return sound[i];
        }

        return Block.soundTypeStone;
    }

    /**
     * The Texture wish you liked to register does not exists.
     */
    public class NoSuchTexture extends Exception {
        private NoSuchTexture(String textName) {
            super(String.format("%s/%s: Texture %s is missing!", name, type, textName), null, false, true);
        }
        private NoSuchTexture(String textName, Throwable cause) {
            super(String.format("%s/%s: Texture %s is missing!", name, type, textName), cause, false, true);
        }
    }
}
