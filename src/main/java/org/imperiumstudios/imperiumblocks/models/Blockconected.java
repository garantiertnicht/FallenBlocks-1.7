/*
 * Copyright (c) 2016, garantiertnicht
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by garantiertnicht Weichware.
 * 4. Neither the name of garantiertnicht Weichware nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY GARANTIERTNICHT WEICHWARE ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GARANTIERTNICHT WEICHWARE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.imperiumstudios.imperiumblocks.models;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import org.imperiumstudios.imperiumblocks.BlockUtils;
import org.imperiumstudios.imperiumblocks.ImperiumBlocks;
import org.imperiumstudios.imperiumblocks.conection.ConectionMatrix;
import org.imperiumstudios.imperiumblocks.conection.ConectionMatrixSide;
import org.imperiumstudios.imperiumblocks.renderer.SideRotatable;

import java.util.Properties;

public class Blockconected extends net.minecraft.block.Block implements GenericBlock, SideRotatable {
    BlockUtils blockUtils;
    IIcon[] icons = new IIcon[6];

    public Blockconected(Properties blockProps, BlockUtils blockUtils) {
        super(blockUtils.getMaterial(blockProps.getProperty("material", "rock")));

        this.blockUtils = blockUtils;
        this.setCreativeTab(ImperiumBlocks.blockTab);
    }

    public void registerBlockIcons(IIconRegister iconReg) {
        try {
            icons = blockUtils.registerIcons(iconReg, "none", "one", "parallel", "edge", "three", "all");
            blockIcon = icons[0];
        } catch (BlockUtils.NoSuchTexture exc) {
            ImperiumBlocks.log.warn(exc.getMessage());
        }
    }

    @Override
    public Item getItem() {
        return null;
    }

    private ConectionMatrixSide getConectionMatrixSide(IBlockAccess blockAccess, int x, int y, int z, int side) {
        boolean up = blockAccess.getBlock(x, y + 1, z) == this;
        boolean down = blockAccess.getBlock(x, y - 1, z) == this;
        boolean east = blockAccess.getBlock(x + 1, y, z) == this;
        boolean west = blockAccess.getBlock(x - 1, y, z) == this;
        boolean south = blockAccess.getBlock(x, y, z + 1) == this;
        boolean north = blockAccess.getBlock(x, y, z - 1) == this;

        ConectionMatrix conectionMatrix = new ConectionMatrix(down, up, north, south, west, east);

        switch(side) {
            case /* down */ 0:
                return conectionMatrix.down();
            case /* up */ 1:
                return conectionMatrix.up();
            case /*north*/ 2:
                return conectionMatrix.north();
            case /* south */ 3:
                return conectionMatrix.south();
            case /* west */ 4:
                return conectionMatrix.west();
            case /* east */ 5:
                return conectionMatrix.east();
            default:
                return null;
        }
    }

    @Override
    public int getRenderType() {
        return ImperiumBlocks.proxy.getRotatableSideRenderer();
    }

    @Override
    public IIcon getIcon(IBlockAccess blockAccess, int x, int y, int z, int side) {
        return icons[getConectionMatrixSide(blockAccess, x, y, z, side).icon];
    }

    @Override
    public int getRotationForSide(IBlockAccess blockAccess, int x, int y, int z, int side) {
        return getConectionMatrixSide(blockAccess, x, y, z, side).rotation;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float clickX, float clickY, float clickZ) {
        if(!blockUtils.devLog) {
            return false;
        }

        ConectionMatrixSide conections = getConectionMatrixSide(world, x, y, z, side);
        player.addChatComponentMessage(new ChatComponentText(String.format("texture=%d rotation=%d", conections.icon, conections.rotation)));
        player.addChatComponentMessage(new ChatComponentText(String.format("up=%b down=%b left=%b right=%b", conections.up, conections.down, conections.left, conections.right)));

        boolean up = world.getBlock(x, y + 1, z) == this;
        boolean down = world.getBlock(x, y - 1, z) == this;
        boolean east = world.getBlock(x + 1, y, z) == this;
        boolean west = world.getBlock(x - 1, y, z) == this;
        boolean south = world.getBlock(x, y, z + 1) == this;
        boolean north = world.getBlock(x, y, z - 1) == this;

        player.addChatComponentMessage(new ChatComponentText(String.format("up=%b down=%b north=%b south=%b east=%b west=%b", up, down, east, west, south, north)));

        return true;
    }
}
