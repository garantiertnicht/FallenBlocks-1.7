/*
 * Copyright (c) 2017, garantiertnicht
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by garantiertnicht Weichware.
 * 4. Neither the name of garantiertnicht Weichware nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY GARANTIERTNICHT WEICHWARE ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GARANTIERTNICHT WEICHWARE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.imperiumstudios.imperiumblocks;

import java.util.LinkedList;
import java.util.function.Consumer;

import org.apache.logging.log4j.Logger;

public class Addon {
    private boolean model;
    protected static LinkedList<Consumer<Event.FileLoaded>> fileLoaded = new LinkedList<>();
    protected static LinkedList<Consumer<Event.BlockProcessed>> blockproccessed = new LinkedList<>();

    public class EventHelper<T> {
        private Consumer<T> consumer;

        private EventHelper(Consumer<T> consumer) {
            this.consumer = consumer;
        }

        public Consumer<T> getConsumer() {
            return consumer;
        }
    }

    public Addon() {
        model = false;
    }

    public ModelRegister createModelRegister(String pkg) {
        if(model) throw new IllegalStateException("Any Addon may ony request ONE Model Register!");
        return new ModelRegister(pkg);
    }

    public void addFileLoadedEventListener(Consumer<Event.FileLoaded> listener) {
        fileLoaded.add(listener);
    }

    public void addBlockProccessedEventListener(Consumer<Event.BlockProcessed> listener) {
        blockproccessed.add(listener);
    }

    public static String getModId() {
        return ImperiumBlocks.MODID;
    }

    public static LinkedList<Consumer<Event.FileLoaded>> getFileLoaded() {
        return fileLoaded;
    }

    public static LinkedList<Consumer<Event.BlockProcessed>> getBlockproccessed() {
        return blockproccessed;
    }

    public Logger getLogger() {
        return ImperiumBlocks.log;
    }
}
