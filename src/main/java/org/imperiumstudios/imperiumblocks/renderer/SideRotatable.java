package org.imperiumstudios.imperiumblocks.renderer;

import net.minecraft.world.IBlockAccess;

public interface SideRotatable {
    int getRotationForSide(IBlockAccess world, int x, int y, int z, int side);
}
