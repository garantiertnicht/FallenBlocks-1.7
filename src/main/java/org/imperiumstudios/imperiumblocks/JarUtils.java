/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.imperiumstudios.imperiumblocks;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarUtils {
    public String[] getResourceFolderContent(String path) throws URISyntaxException, IOException {
        URL dirURL = this.getClass().getClassLoader().getResource(path);

        if(dirURL != null && dirURL.getProtocol().equals("file")) return new File(dirURL.toURI()).list();
        if(dirURL == null) {
            String me = this.getClass().getName().replace(".", "/") +".class";
            dirURL = this.getClass().getClassLoader().getResource(me);
        }

        if(dirURL.getProtocol().equals("jar")) {
            String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!"));
            JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
            Enumeration<JarEntry> entries = jar.entries();
            Set<String> result = new HashSet<String>();

            while(entries.hasMoreElements()) {
                String name = entries.nextElement().getName();

                if(name.startsWith(path)) {
                    String entry = name.substring(path.length());

                    if(entry.length() == 0) continue;
                    if(entry.charAt(0) == '/') entry = entry.substring(1);
                    if(entry.length() == 0) continue;

                    int checkSubdir = entry.indexOf("/");

                    if(checkSubdir >= 0) entry = entry.substring(0, checkSubdir);
                    if(entry.length() != 0)result.add(entry);
                }
            }

            jar.close();

            return result.toArray(new String[result.size()]);
        } else {
            throw new UnsupportedOperationException("Cannot list files for URL " + dirURL);
        }
    }

}
